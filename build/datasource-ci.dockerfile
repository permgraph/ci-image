FROM docker.io/golang:1.16-alpine

RUN apk add -U --no-cache \
        yarn \
        docker \
        findutils \
        git \
        git-lfs \
        ca-certificates \
        graphviz \
    && wget -O- -nv https://install.goreleaser.com/github.com/goreleaser/goreleaser.sh | sh -s -- -b /usr/bin \
    && wget -qO- https://github.com/magefile/mage/releases/download/v1.11.0/mage_1.11.0_Linux-64bit.tar.gz | tar --exclude LICENSE -xvz -C /usr/bin/ \
    && wget -O- -nv https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b /usr/local/bin
